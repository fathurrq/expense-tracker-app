### Setup .env file

```js
provided on .env.example file
MONGO_URI=...
SESSION_SECRET=...
```
### install dependencies on backend

```shell
cd backend && npm install
```
### install dependencies on frontend

```shell
cd frontend && npm install
```
### Start the backend server

```shell
npm start
```
### Start the frontend app

```shell
npm run dev
```