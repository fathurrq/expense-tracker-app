import * as XLSX from 'xlsx';
export const exportToExcel = (data) => {
    const formattedData = formatData(data)
    // Create a new workbook and a worksheet
    const workbook = XLSX.utils.book_new();
    const worksheet = XLSX.utils.aoa_to_sheet(formattedData);

    // Add the worksheet to the workbook
    XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet1");

    // Generate an Excel file and trigger the download
    XLSX.writeFile(workbook, "ExportedData.xlsx");
};
function formatData(data) {
    // Create a new array and start with the headers
    const formattedData = [
        ['Date', 'Description', 'Category', 'Payment Type', 'Amount', 'Location']
    ];

    // Iterate over each transaction in the data
    data?.transactions?.forEach(transaction => {
        // Extract the required fields
        const { date, description, category, paymentType, amount, location } = transaction;
        // Convert date from epoch to human-readable format (optional)
        const formattedDate = new Date(parseInt(date)).toLocaleDateString("en-US");
        // Append this transaction to the formatted data array
        formattedData.push([formattedDate, description, category, paymentType, amount, location]);
    });

    return formattedData;
}
